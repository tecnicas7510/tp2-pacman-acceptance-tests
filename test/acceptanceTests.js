const expect = require("chai").expect;
const should = require('should');
const assert = require('assert');

var pacmanTestDriver = require('tp2-pacman-testdriver');

describe("GHOST Functionalities", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });


    describe('A SEARCHER hunter ghost with full vision', function () {

        const maze = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0],
            [0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0],
            [0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
            [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
        const impassableTypesTest = [0, 2];
        const options = {
            random: [pacmanTestDriver.RANDOM.TOP],  // Para los tests el random siempre devuelve TOP, Se itera este array coom random
            iterations: 1,  // cantidad de iteraciones a ejecutar para luego comparar el estado de los fantasmas
            vision: 1000
        }
        const pacmanInitialPosition = {
            columnIndex: 1,
            rowIndex: 1,
            direction: pacmanTestDriver.COMPASS.WEST,
            nextMovements: []
        };
        const ghostInitialPosition = {
            columnIndex: 5,
            rowIndex: 5,
            direction: pacmanTestDriver.COMPASS.NORTH,
            ghostType: pacmanTestDriver.GHOST_TYPE.SEARCHER
        };

        xit('should after 1 movement, continue in his current direction until arrive to a bifurcation', function () {

            options.iterations = 1;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 5);
            assert(result.ghostPosition.rowIndex == 4);
        });

        xit('should after 2 movements, continue NORTH in the bifurcation because is the shortest way to the pacman position', function () {

            options.iterations = 2;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 5);
            assert(result.ghostPosition.rowIndex == 3);
        });

        xit('should after 3 movements, continue in his current direction until arrive to a bifurcation', function () {

            options.iterations = 3;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 5);
            assert(result.ghostPosition.rowIndex == 2);

        });

        xit('should after 4 movements, continue WEST in the bifurcation because is the shortest way to the pacman position', function () {

            options.iterations = 4;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 4);
            assert(result.ghostPosition.rowIndex == 2);

        });

        xit('should after 5 movements, continue in his way until arrive to a bifurcation', function () {

            options.iterations = 5;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 3);
            assert(result.ghostPosition.rowIndex == 2);

        });

        xit('should after 6 movements, continue in his way until arrive to a bifurcation', function () {

            options.iterations = 6;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 3);
            assert(result.ghostPosition.rowIndex == 1);

        });

        xit('should after 7 movements, continue in his way until arrive to a bifurcation', function () {

            options.iterations = 7;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 2);
            assert(result.ghostPosition.rowIndex == 1);

        });

        xit('should after 8 movements, catch the pacman position', function () {

            options.iterations = 8;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 1);
            assert(result.ghostPosition.rowIndex == 1);

        });
    });
});


