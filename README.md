# tp2-pacman.acceptance-tests

In this test suite we will be adding acceptance test for TP2-Pacman
This test suite uses a test driver facade to implement tests decoupling the implementation of each group.
This facade (https://gitlab.com/tecnicas7510/tp2-pacman-testdriver/) should be implemented for each group.


driveCase: is a Function that receives an object configuration of the maze, pacman and ghost positions and returns an object with the state of the game after the iterations configured
COMPASS: is an object with the possible movements or directions.
RANDOM: is an object with the possible movements to use for Random Dummy implementation to be used in the tests.
 GHOST_TYPE: is an object with the possibles Ghost types.

It could be a dummy implementation:

`const pacmanTestDriver = {
    driveCase: function () {
        return {
            ghostPosition: {
                columnIndex: 1,
                rowIndex: 1
            }
        }
    },
    COMPASS : {
        NORTH: "NORTH",
        SOUTH: "SOUTH",
        WEST: "WEST",
        EAST: "EAST"
    },
    RANDOM : {
        TOP: "TOP",
        BOTTOM: "BOTTOM",
        WEST: "WEST",
        EAST: "EAST"
    },
    GHOST_TYPE : {
        SEARCHER: "SEARCHER",
        TEMPERAMENTAL_SEARCHER: "TEMPERAMENTAL_SEARCHER",
        LAZY: "LAZY"
    }
};`
